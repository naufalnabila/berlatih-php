<?php

function ubah_huruf($string){// ord dan chr 97-123
    $hasil = "";
    for($i = 0; $i < strlen($string);$i++ ){
        $a = ord($string[$i])+1;
        $b = chr($a);
        $hasil = $hasil.$b;
    }
    return $hasil."<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>